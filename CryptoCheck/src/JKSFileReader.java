import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;

public class JKSFileReader {
    public JKSFileReader() {
    }
/*
    public static PrivateKey getClientPrivateKey() {
        return readClientPrivateKeyFromKeyStore();
    }

    public static PublicKey getCheckoutPublicKey() {
        return readCheckoutPublicKeyFromKeyStore();
    }
*/
    public static PrivateKey readClientPrivateKeyFromKeyStore(String storePass, String alias, String pass, String path){
        PrivateKey clientPrivateKey = null;
        try {
            String keyStorePasswd = storePass;
            String keyAlias = alias;
            String keyPassword = pass;
            String keyStorePath = path;
            KeyStore ks = readJKSKeystore(keyStorePath, keyAlias, keyStorePasswd);
            System.out.println(ks);
            clientPrivateKey = (PrivateKey)ks.getKey(keyAlias, keyPassword.toCharArray());
            System.out.println(clientPrivateKey);
            return clientPrivateKey;
        } catch (Exception var6) {
            var6.printStackTrace();
        }
        return clientPrivateKey;
    }

    public static PublicKey readCheckoutPublicKeyFromKeyStore(String storePass, String path, String alias){
        PublicKey alacPublicKey = null;
        try {
            String keyStorePath = path;
            String keyStorePasswd = storePass;
            String keyStoreAlias = alias;
            KeyStore ks = readJKSKeystore(keyStorePath, keyStoreAlias, keyStorePasswd);
            Certificate certificate = ks.getCertificate(keyStoreAlias);
            alacPublicKey = certificate.getPublicKey();
            return alacPublicKey;
        } catch (Exception var6) {
            var6.printStackTrace();
        }
        return alacPublicKey;
    }

    public static KeyStore readJKSKeystore(String keyStorePath, String keyAlias, String keyStorePasswd){
        InputStream inputStream = null;
        KeyStore ks = null;

        try {
            ks = KeyStore.getInstance("JKS");

            try {
                inputStream = JKSFileReader.class.getClassLoader().getResourceAsStream(keyStorePath);
            } catch (Exception var14) {
                var14.printStackTrace();
            }

            System.out.println("inputStream is " + inputStream);
            if (inputStream == null) {
                System.out.println("Reading keystore from file path");
                inputStream = new FileInputStream(keyStorePath);
                System.out.println("inputStream " + inputStream);
            }

            ks.load((InputStream)inputStream, keyStorePasswd.toCharArray());
        } catch (Exception var15) {
            var15.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    ((InputStream)inputStream).close();
                } catch (IOException var13) {
                    var13.printStackTrace();
                }
            }

        }

        return ks;
    }

    private static byte[] readFully(InputStream stream){
        byte[] buffer = new byte[819288];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        int bytesRead;
        try {
            while((bytesRead = stream.read(buffer)) != -1) {
                baos.write(buffer, 0, bytesRead);
            }
        } catch (IOException var5) {
            var5.printStackTrace();
        }

        return baos.toByteArray();
    }
}
