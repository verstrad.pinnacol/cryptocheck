import com.alacriti.checkout.api.FundingAccount;
import com.alacriti.checkout.*;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;

public class Main {
        public static String token = "ekMXYBsg6XePMN2YIDBjWOriZsND4PuKekWbJqnve8yziyPlU2E4xmmAxLq7z2McnpjDenBuOdGruUpCtC9XZzZSikL+7FNkEvZoNje4IMZPA1h/Vg/3/7hVuZdOuYhXydxy0Xg1Jbe3XEJRyD563R88NMHkWeq+O24qRhEawCMEi4gRUU8AMCyoR8e7Drn89BNPaiRT6ljBRG3byZ/CbiK3nBBO6yQrG6aJm67Ewe5O6PDVZ4emI9DbZMhmcpvB7CGIdTHN8jEvT/4tpqsW3T7GFGCRQKIWEy69RFsgLsmHIllJiUeHPLIB9dxdReUrNn8UnVhctAFXQfdSLZSTsA==";
        public static String digisign = "eGVzjmALouMuhmcFiYAG+weUH/JobQbHUdrfMbBumIKEH95hKdCiwgqPKSJBjqrxc4myZg0ygV88nK5bgRaGAI/8rog36ikUw7LqfDq1bkVb8nFbmLifxNz70BSMR8UM5mIEY3x7Azr7ajkM346MNDSr8p96IhpVXI2Sd///zCdPir9tKkzKp0jwsenyfPxoNjSaSES4FMerj3r0EHjj3zsuohYKv2Ii6hsJcdCrtADT21fEVCis9s8TUDgjzGS+17d7B+5AHoz3HGA82GMlbaY8oe97GH28NTZlg2uPtME8gZrKHd+FtC8xFJ5PG9aLpOSneHoYqaynOJQagA21pw==";
        
        public static void main(String[] args) throws Exception {

            /*
            // Read in the key into a String
            StringBuilder pkcs8Lines = new StringBuilder();
            BufferedReader rdr = new BufferedReader(new StringReader(PRIVATE_KEY));
            String line;
            while ((line = rdr.readLine()) != null) {
                pkcs8Lines.append(line);
            }

            // Remove the "BEGIN" and "END" lines, as well as any whitespace

            String pkcs8Pem = pkcs8Lines.toString();
            pkcs8Pem = pkcs8Pem.replace("-----BEGIN PRIVATE KEY-----", "");
            pkcs8Pem = pkcs8Pem.replace("-----END PRIVATE KEY-----", "");
            pkcs8Pem = pkcs8Pem.replaceAll("\\s+","");

            // Base64 decode the result

            byte [] pkcs8EncodedBytes = Base64.getDecoder().decode(pkcs8Pem);

            // extract the private key

            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(pkcs8EncodedBytes);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PrivateKey privKey = kf.generatePrivate(keySpec);
            System.out.println(privKey);

            StringBuilder x509PubLines = new StringBuilder();
            BufferedReader reader = new BufferedReader(new StringReader(PUBLIC_KEY));
            String lines;
            while ((lines = reader.readLine()) != null) {
                x509PubLines.append(lines);
            }
            String x509Pub = x509PubLines.toString();
            x509Pub = x509Pub.replace("-----BEGIN PUBLIC KEY-----", "");
            x509Pub = x509Pub.replace("-----END PUBLIC KEY-----", "");
            x509Pub = x509Pub.replaceAll("\\s", "");
            byte[] pubBytes = Base64.getDecoder().decode(x509Pub);

            X509EncodedKeySpec xKeySpec = new X509EncodedKeySpec(pubBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey publicKey = keyFactory.generatePublic(xKeySpec);
            System.out.println(publicKey);

             */

            String privKeyStorePass = "AlacClient6390347460";
            String privKeyPath = "keystores/6390347460.keystore";
            String privKeyPass = "AlacClient6390347460";
            String privAlias = "6390347460";
            String pubKeyStorePass = "COpubkey4clientsandboxenv";
            String pubKeyPath = "keystores/coserver_pub.keystore";
            String pubKeyAlias = "sbcosrv";
            PrivateKey privKey = JKSFileReader.readClientPrivateKeyFromKeyStore(privKeyStorePass,privAlias,privKeyPass,privKeyPath);
            PublicKey publicKey = JKSFileReader.readCheckoutPublicKeyFromKeyStore(pubKeyStorePass,pubKeyPath, pubKeyAlias);

            byte[] tokenBytes = Base64.getDecoder().decode(token);
            System.out.println("decoded bytes " + tokenBytes);
            System.out.println("decoded token = " + tokenBytes.toString());
            byte[] digisignBytes = digisign.getBytes();
            String decryptedToken = decrypt(tokenBytes, privKey);
            System.out.println("decrypted token = " + decryptedToken);
            Boolean verified = verifySign(decryptedToken, publicKey, digisign);
            System.out.println("verified signature = " + verified);
            byte[] encryptedTokenBytes = encrypt(decryptedToken, publicKey);
            System.out.println("encrypted token = " + encryptedTokenBytes.toString());
            String encodedEncryption = Base64.getEncoder().encodeToString(encryptedTokenBytes);
            System.out.println("encoded token = " + encodedEncryption);
            String outgoingDigisign = generateSignature(decryptedToken, privKey);
            System.out.println("outgoing digisign = " + outgoingDigisign);

            String inputString = "POST:/app/opco/v3/service/fundingaccounts/token::client_key=6390347460&digi_sign=fEGWHm0DP6IJyQXXC6x9mg7T+oCjPM0E5zNIi+2VPOoXdm6yw+YQvEPEGUf20R0Yt8LOAN0DwLH/z74jLcd4g4AiTCdl4ilzTkOExnAGPzYVlKYpwlhP+oUYRTpdfmVGroRdKtOjXm0h8YJ2nQ0xNMhd/tVVEw7N29Mr6EWUgfkWxbfwuV349VRfhFyesXpg/uP/YryfFaP7vXjvzstn/3isXc9FqdtjKn4RXpSr0Mpp5AXl1sg3b6Un9xBFzckjOEbFl6GmSILudTs7j8vBM462goXfydPU6PYJJapuRT7j3OpsqUwb9S6MbyRKvJu5gs8wf9H699V7shbUuuGI0g==:{\"funding_account_token\":{\"token\":\"iZ6X7BZ3VJtcsrSPfgxLET1Hgr5nxOm5guz10jjY/qDyvequoNlIaHbEKGzVbBuz5gStT0RPy0jvFGo2HgJlWmdTEkuTFIS51qmIKoGpi+FWQDryKy5elZnZcgm/sJf+cEIrZL7rndisC3tu3q3PlQxaSHJyTmZ4R7YAkFWL9C+lLSA6DAJZhR1464UOaTc0qqh9giC/KlrycOed+p3IvczvyUdxcrnrT7dl7CyYbhsh75OyJFCf/JrKncq89S+Bv33HSAB3TLL5Ij0N5q5BIgw0lc28ZBsJE/fEQqAGKJ8CjOLKQ0ZR1cBV+dvEvPMOYjADRQFYzNcpP+w9nAm+0g==\"}}";
            String computedHash = decryptAndEncodeData("X8KF4VBQVYY8BW5M", inputString);
            System.out.println("computed hash = " + computedHash);

            //FundingAccount fundingAccount = new FundingAccount().withToken(token, digisign).forClient(privAlias, "X8KF4VBQVYY8BW5M").add();

            //System.out.println("funding account " + fundingAccount);
        }

        public static String decrypt(byte[] text, PrivateKey key) {
            byte[] decryptedText = null;
            String rsa = "RSA";
            try {
                final Cipher cipher = Cipher.getInstance(rsa);
                cipher.init(Cipher.DECRYPT_MODE, key);
                decryptedText = cipher.doFinal(text);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return new String(decryptedText);
        }

        public static byte[] encrypt(String text, PublicKey key) {
            byte[] cipherText = null;
            try {
                final Cipher cipher = Cipher.getInstance("RSA");
                cipher.init(Cipher.ENCRYPT_MODE, key);
                cipherText = cipher.doFinal(text.getBytes());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return cipherText;
        }

        public static String generateSignature(String plainText, PrivateKey privateKey) {
            byte[] signatureBytes = null;
            try {
                Signature sign = Signature.getInstance("SHA1WithRSA");
                sign.initSign(privateKey);
                sign.update(plainText.getBytes("UTF-8"));
                signatureBytes = sign.sign();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return Base64.getEncoder().encodeToString(signatureBytes);
        }

        public static Boolean verifySign(String plainText, PublicKey publicKey, String digiSign) {
            Boolean verified = false;
            try {
                Signature sign = Signature.getInstance("SHA1WithRSA");
                sign.initVerify(publicKey);
                sign.update(plainText.getBytes("UTF-8"));
                verified = sign.verify(Base64.getDecoder().decode(digiSign));
                return verified;
            } catch (Exception var5) {
                var5.printStackTrace();
            }
            return verified;
        }
/*
        public static String computeHMACSHA256Hash(String path, String serviceURL, String method, Map<String, String> queryParams, Map<String, String> headerParams, Object body, JSON json, String signatureKey) throws ApiException {
            path = serviceURL + path;
            path = path.substring(path.indexOf("/app"));
            StringBuilder sb = new StringBuilder();
            sb.append((method.toUpperCase() + ":").trim());
            sb.append((path + ":").trim());
            //sb.append(getQueryString(queryParams));
            sb.append(getHeaders(headerParams));
            String jsonBody = json.serialize(body);
            if (jsonBody != null && !"".equals(jsonBody) && !"null".equals(jsonBody)) {
                sb.append(jsonBody.trim());
            }

            String hash = decryptAndEncodeData(signatureKey, sb.toString().trim());
            return hash;
        }
*/
        public static String decryptAndEncodeData(String secret, String message){
            String computedHash = "";

            try {
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                computedHash = Base64.getEncoder().encodeToString(sha256_HMAC.doFinal(message.getBytes()));
                return computedHash.trim();
            } catch (Exception var5) {
                var5.printStackTrace();
            }
            return computedHash.trim();
        }
/*
    public static String getQueryString(List<Pair> queryParams) {
        String queryString = "";
        ArrayList<String> sortedList = new ArrayList();
        Map<String, String> localMap = new HashMap();
        Iterator var4 = queryParams.iterator();

        while(var4.hasNext()) {
            Pair pairObj = (Pair)var4.next();
            if (pairObj.getValue() != null) {
                sortedList.add(pairObj.getName());
                localMap.put(pairObj.getName(), pairObj.getValue());
            }
        }

        Collections.sort(sortedList);
        int listSize = sortedList.size();
        int i = 0;

        for(Iterator var6 = sortedList.iterator(); var6.hasNext(); ++i) {
            String key = (String)var6.next();
            if (i != listSize - 1) {
                queryString = queryString + key + "=" + (String)localMap.get(key) + "&";
            } else {
                queryString = queryString + key + "=" + (String)localMap.get(key);
            }
        }

        return queryString + ":";
    }
*/
    public static String getHeaders(Map<String, String> headersParamMap) {
        String headers = "";
        ArrayList<String> sortedList = new ArrayList();
        if (headersParamMap != null && headersParamMap.size() > 0) {
            Iterator var3 = headersParamMap.keySet().iterator();

            while(var3.hasNext()) {
                String key = (String)var3.next();
                if (headersParamMap.get(key) != null) {
                    sortedList.add(key);
                }
            }
        }

        Collections.sort(sortedList);
        int listSize = sortedList.size();
        int i = 0;

        for(Iterator var5 = sortedList.iterator(); var5.hasNext(); ++i) {
            String key = (String)var5.next();
            if (i != listSize - 1) {
                headers = headers + key + "=" + (String)headersParamMap.get(key) + "&";
            } else {
                headers = headers + key + "=" + (String)headersParamMap.get(key);
            }
        }

        return headers + ":";
    }
    }
